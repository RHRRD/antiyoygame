package game;

public class Towers {

    private Tower[] towers;

    public Towers() {
        towers = new Tower[]{
                new Tower(15, 1),
                new Tower(35, 6)
        };
    }

    public Tower[] getTowers() {
        return towers;
    }
}
