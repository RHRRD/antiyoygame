package game.step;

import game.Field;

public interface Steps {

    boolean canStep(Field field);

    void doStep(Field field);

}
