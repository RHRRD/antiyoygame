package game.step;

import game.Field;

public class StepBuyUnit implements Steps {

    @Override
    public boolean canStep(Field field) {
        return field.getMoneyCount() >= field.getUnits().getUnits()[0].getCost();
    }

    @Override
    public void doStep(Field field) {
        field.increaseUnitCount();
        field.increaseUnitCountCanStep();
        field.increaseMoneyCount(-field.getUnits().getUnits()[0].getCost());
        field.increaseIncome(-field.getUnits().getUnits()[0].getIncomeMinus());
        field.increaseStep();
    }

    @Override
    public String toString() {
        return "buyUnit";
    }
}
