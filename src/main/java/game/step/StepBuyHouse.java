package game.step;

import game.Field;

public class StepBuyHouse implements Steps {

    @Override
    public boolean canStep(Field field) {
        return field.getMoneyCount() >= field.getHouse().getCost();
    }

    @Override
    public void doStep(Field field) {
        field.increaseHouseCount();
        field.increaseMoneyCount(-field.getHouse().getCost());
        field.getHouse().increaseCostByStep();
        field.increaseIncome(field.getHouse().getIncomePlus());
        field.increaseStep();
    }

    @Override
    public String toString() {
        return "buyHouse";
    }

}
