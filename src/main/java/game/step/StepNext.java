package game.step;

import game.Field;

public class StepNext implements Steps {

    @Override
    public boolean canStep(Field field) {
        return true;
    }

    @Override
    public void doStep(Field field) {
        field.increaseMoneyCount(field.getIncome());
        field.increaseIncome(field.getUnitCount());
        for (int i = 0; i < field.getUnitCount(); i++) {
            field.increaseFieldCount();
        }
        field.setUnitCountCanStep(field.getUnitCount());
        field.increaseStep();
    }

    @Override
    public String toString() {
        return "next";
    }
}
