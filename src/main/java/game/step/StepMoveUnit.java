package game.step;

import game.Field;

public class StepMoveUnit implements Steps {

    @Override
    public boolean canStep(Field field) {
        return field.getUnitCountCanStep() != 0;
    }

    @Override
    public void doStep(Field field) {
        field.increaseIncome(1);
        field.increaseFieldCount();
        field.decreaseUnitCountCanStep();
        field.increaseStep();
    }

    @Override
    public String toString() {
        return "moveUnit";
    }

}
