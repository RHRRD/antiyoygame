package game;

public class Field {

    private final Units units = new Units();
    private final Towers towers = new Towers();
    private final House house = new House();
    private int fieldCount;
    private int houseCount;
    private int unitCount;
    private int unitCountCanStep;
    private int moneyCount;
    private int income;
    private int step;

    public Field() {
        fieldCount = 5;
        houseCount = 1;
        unitCount = 0;
        unitCountCanStep = 0;
        moneyCount = 10;
        income = 5;
        step = 0;
    }

    private Field(int fieldCount, int houseCount, int unitCount, int unitCountCanStep, int moneyCount, int income, int step) {
        this.fieldCount = fieldCount;
        this.houseCount = houseCount;
        this.unitCount = unitCount;
        this.unitCountCanStep = unitCountCanStep;
        this.moneyCount = moneyCount;
        this.income = income;
        this.step = step;
    }

    public Field clone() {
        return new Field(fieldCount, houseCount, unitCount, unitCountCanStep, moneyCount, income, step);
    }

    public int getFieldCount() {
        return fieldCount;
    }

    public void increaseFieldCount() {
        this.fieldCount++;
    }

    public int getHouseCount() {
        return houseCount;
    }

    public void increaseHouseCount() {
        this.houseCount++;
    }

    public int getUnitCount() {
        return unitCount;
    }

    public void increaseUnitCount() {
        this.unitCount++;
    }

    public int getUnitCountCanStep() {
        return unitCountCanStep;
    }

    public void setUnitCountCanStep(int unitCountCanStep) {
        this.unitCountCanStep = unitCountCanStep;
    }

    public void decreaseUnitCountCanStep() {
        this.unitCountCanStep--;
    }

    public void increaseUnitCountCanStep() {
        this.unitCountCanStep++;
    }

    public int getMoneyCount() {
        return moneyCount;
    }

    public void increaseMoneyCount(int moneyCount) {
        this.moneyCount += moneyCount;
    }

    public int getIncome() {
        return income;
    }

    public void increaseIncome(int income) {
        this.income += income;
    }

    public int getStep() {
        return step;
    }

    public void increaseStep() {
        this.step++;
    }

    public Units getUnits() {
        return units;
    }

    public Towers getTowers() {
        return towers;
    }

    public House getHouse() {
        return house;
    }

    @Override
    public String toString() {
        return "Field{" +
                "fieldCount=" + fieldCount +
                ", houseCount=" + houseCount +
                ", unitCount=" + unitCount +
//                ", allCount=" + (unitCount + houseCount) +
                ", moneyCount=" + moneyCount +
                ", income=" + income +
                ", step=" + step +
//                ", units=" + units +
//                ", towers=" + towers +
//                ", house=" + house +
                '}';
    }
}
