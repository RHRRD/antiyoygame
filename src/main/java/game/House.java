package game;

public class House {

    private int cost;
    private final int incomePlus;
    private final int increaseCostByStep;

    public House() {
        this.cost = 12;
        this.incomePlus = 4;
        this.increaseCostByStep = 2;
    }

    public int getCost() {
        return cost;
    }

    public int getIncomePlus() {
        return incomePlus;
    }

    public void increaseCostByStep() {
        cost += increaseCostByStep;
    }

}
