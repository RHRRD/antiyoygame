package game;

public class Unit {

    private int cost;
    private int incomeMinus;

    public Unit(int cost, int incomeMinus) {
        this.cost = cost;
        this.incomeMinus = incomeMinus;
    }

    public int getCost() {
        return cost;
    }

    public int getIncomeMinus() {
        return incomeMinus;
    }
}
