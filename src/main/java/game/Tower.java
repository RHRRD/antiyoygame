package game;

public class Tower {

    private final int cost;
    private final int incomeMinus;

    public Tower(int cost, int incomeMinus) {
        this.cost = cost;
        this.incomeMinus = incomeMinus;
    }

    public int getCost() {
        return cost;
    }

    public int getIncomeMinus() {
        return incomeMinus;
    }
}
