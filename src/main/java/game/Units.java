package game;

public class Units {

    private Unit[] units;

    public Units() {
        units = new Unit[]{
                new Unit(10, 2),
                new Unit(20, 6),
                new Unit(30, 18),
                new Unit(40, 36)
        };
    }

    public Unit[] getUnits() {
        return units;
    }
}
