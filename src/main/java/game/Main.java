package game;

import game.step.StepBuyHouse;
import game.step.StepBuyUnit;
import game.step.StepMoveUnit;
import game.step.StepNext;
import game.step.Steps;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Field startField = new Field();
        Steps stepNext = new StepNext();
        Steps stepBuyHouse = new StepBuyHouse();
        Steps stepBuyUnit = new StepBuyUnit();
        Steps stepMoveUnit = new StepMoveUnit();
        Steps[] allStep = new Steps[]{stepNext, stepBuyHouse, stepBuyUnit, stepMoveUnit};

        print(startField);

        Stack<Steps> steps = new Stack<>();

        execute(3, steps, startField, allStep);
    }

    static void execute(int length, Stack<Steps> steps, Field field, Steps[] allStep) {
        for (int i = 0; i < allStep.length; i++) {
            Field prevField = field.clone();
            if (!doStep(allStep[i], field)) {
                continue;
            }
            steps.push(allStep[i]);

            if (steps.size() == length) {
                print(steps, field);
            } else {
                execute(length, steps, field, allStep);
            }
            steps.pop();
            field = prevField;
        }
    }

    static boolean doStep(Steps steps, Field field) {
        if (!steps.canStep(field)) {
            return false;
        }
        steps.doStep(field);
        return true;
    }

    static void print(Stack<Steps> steps, Field field) {
        System.out.println(steps.toString() + " " + field);
    }

    static void print(Field field) {
        System.out.println("[] " + field);
    }

}
